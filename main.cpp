#include <X11/Xos.h>
#include <X11/Xutil.h>
#include <X11/Xlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <math.h>

using namespace std;

vector<string> d;
string command;	
string filename;
string version;


void Load(void);
void Do(void);
void Save(void);

bool lp1;
bool lp2;

bool del;

bool loaded;

void Do(void)
{
	if (command == "q")
	{
		exit(EXIT_SUCCESS);
	}
	else if ((command[0] == 'e')&&(command[1] == ' '))
	{
		filename = command.substr(2);
		cout << "Loaded" << endl;
		loaded = 1;
		Load();
	}
	else if (command == "s")
	{
		Save();
	}
	else if (command == "l")
	{
		lp1 = 1;
		lp2 = 0;
		del = 0;
	}
	else if (command == "d")
	{
		del = 1;
		lp1 = 0;
		lp2 = 0;
	}
	cout << command << endl;
	fflush(stdout);
}

void Load(void)
{
	ifstream in;
	string s;
	int n;
	in.open(filename.c_str());
	
	d.clear();
	in >> version;
	in >> n;
	getline(in, s);
	for (int i = 0; i <= n; i++)
	{
		getline(in, s);
		d.push_back(s);
		cout << s << endl;
		s = "";
		fflush(stdout);
	}
	in.close();
}

void Save(void)
{
	ofstream out;
	out.open(filename.c_str());
	out << version << endl;
	out << d.size() << endl;
	for (int i = 0; i < d.size(); i++)
	{
		out << d[i] << endl;
	}
	out.close();
}


bool Belong(string line, int a, int b)
{
	istringstream out(line.substr(5));
	int x1, y1, x2, y2;
	out >> x1 >> y1 >> x2 >> y2;
	cout << x1 << endl 
	     << y1 << endl
	     << x2 << endl
	     << y2 << endl;
	cout << endl;
	cout << a << endl
	     << b << endl;
	for (double t = 0; t <= 1; t += 1.0/(abs(x2 - x1) + abs(y2 - y1)))
	{
		double x = x1 + t*(x2 - x1);
		double y = y1 + t*(y2 - y1);
		if ((a - x)*(a - x) + (b - y)*(b - y) < 3)
		{
			return 1;
		}
	}
	return 0;
}

int main(void)
{
	command = "";
	filename = "new.gpf";
	version = "1.1";
	loaded = 0;
	lp1 = 0;
	lp2 = 0;
	del = 0;

	d.clear();

	Display *disp;
	disp = XOpenDisplay(NULL);

	int screen = XDefaultScreen(disp);

	Window root = XRootWindow(disp, screen);

	unsigned long black = XBlackPixel(disp, screen);
	unsigned long white = XWhitePixel(disp, screen);

	Window wind = XCreateSimpleWindow
		(
			disp, root,
			0, 0, 500, 500, 0,
			black, white
		);

	XSelectInput(disp, wind, KeyPressMask | ButtonPressMask | ExposureMask);
	
	XMapWindow(disp, wind);

	// GC init
	XGCValues value;	
	unsigned long valuemask;
	GC gc;

	value.foreground = 0;
	value.line_width = 3;
	value.fill_style = FillSolid;
	value.line_style = LineSolid;

	valuemask = GCLineStyle | GCLineWidth | GCFillStyle | GCForeground;

	gc = XCreateGC(disp, wind, valuemask, &value);
	// end

	while(1)
	{
		if(loaded)
		{
			int i;
			XClearWindow(disp, wind);
			for (i = 0; i < d.size(); i++)
			{
				if ((d[i].size() > 4)&&(d[i].substr(0, 4) == "line"))
				{
					istringstream cur(d[i].substr(5));
					int x1, x2, y1, y2;
					cur >> x1 >> y1 >> x2 >> y2;
					XDrawLine(disp, wind, gc, x1, y1, x2, y2);
					XFlush(disp);
				}
			}
			loaded = 0;
		}

		XEvent event;
		XNextEvent(disp, &event);
		
		switch(event.type)
		{
			case Expose:
			{
				/*
				 * Что уже нарисовано
				 */
				int i;
				XClearWindow(disp, wind);
				for (i = 0; i < d.size(); i++)
				{
					if ((d[i].size() > 4)&&(d[i].substr(0, 4) == "line"))
					{
						istringstream cur(d[i].substr(5));
						int x1, x2, y1, y2;
						cur >> x1 >> y1 >> x2 >> y2;
						XDrawLine(disp, wind, gc, x1, y1, x2, y2);
						XFlush(disp);
					}
				}


				string hint = filename + " --- " + command + " ";
				XStoreName(disp, wind, hint.c_str());
				XFlush(disp);
				break;
			}
			case KeyPress:
			{
				/*
				 * Команда
				 */
				if (event.xkey.keycode == 36)
				{
					Do();
					command = "";
				}
				else
				{
					command.push_back((char)XKeycodeToKeysym(disp, event.xkey.keycode, 0));
					string hint = filename + " --- " + command + " ";
					XStoreName(disp, wind, hint.c_str());
					XFlush(disp);
				}
				break;
			}
			case ButtonPress:
			{
				/*
				 * Рисование
				 */
				if ((lp1 == 1)&&(lp2 == 0))
				{
					ostringstream cur;
					cur << "line ";
					cur << event.xbutton.x << " " << event.xbutton.y << " ";
					lp1 = 0;
					lp2 = 1;
					del = 0;
					d.push_back(cur.str());
					break;
				}
				else if((lp1 == 0)&&(lp2 == 1))
				{
					ostringstream cur;
					cur << d[d.size() - 1];
					cur << event.xbutton.x << " " << event.xbutton.y;
					d[d.size() - 1] = cur.str();
					lp1 = 1;
					lp2 = 0;
					del = 0;
					int i;
					XClearWindow(disp, wind);
					for (i = 0; i < d.size(); i++)
					{
						if ((d[i].size() > 4)&&(d[i].substr(0, 4) == "line"))
						{
							istringstream cur(d[i].substr(5));
							int x1, x2, y1, y2;
							cur >> x1 >> y1 >> x2 >> y2;
							XDrawLine(disp, wind, gc, x1, y1, x2, y2);
							XFlush(disp);
						}
					}


					break;
				}
				else if(del == 1)
				{
					int a = event.xbutton.x;
					int b = event.xbutton.y;
					for (int i = 0; i < d.size(); i++)
					{
						if (Belong(d[i], a, b))
						{
							d.erase(d.begin() + i);
							break;
						}
					}

					int i;
					XClearWindow(disp, wind);
					for (i = 0; i < d.size(); i++)
					{
						if ((d[i].size() > 4)&&(d[i].substr(0, 4) == "line"))
						{
							istringstream cur(d[i].substr(5));
							int x1, x2, y1, y2;
							cur >> x1 >> y1 >> x2 >> y2;
							XDrawLine(disp, wind, gc, x1, y1, x2, y2);
							XFlush(disp);
						}
					}
				}
			}
		}
	}
	return 0;
}
